import {Button, Row, Col, Container} from 'react-bootstrap'
import {Link} from 'react-router-dom';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";




export default function Footer(){
	return(


  <div>
	<Row id="footer-info1" className=" pt-5">
        <Col xs={12} md={3}>ABOUT US</Col>
        <Col xs={12} md={3}>PARTY PACKAGES</Col>
        <Col xs={12} md={3}>FRANCHISING & CAREERS</Col>
        <Col xs={12} md={3}>CONTACT US</Col>
      	</Row>
 
    <Row id="footer-info" className=" pt-0">
        <Col xs={12} md={3}></Col>
        <Col xs={12} md={3}></Col>
        <Col xs={12} md={3}></Col>
        <Col xs={12} md={3}></Col>
    </Row>
   
    
	<Row>
			<Col id="footer" className="text-center">
				<p> Kuya Eds | Roasted & Fried © 2022</p>
			</Col>
	</Row>

</div>
	)
}


