import {Row, Col, Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';


export default function ProductCard({product}){

  const {name, description, price, _id} = product


  return (
   
    
    
    <Row id="product-card" className="mt-3 mb-3">

      
      <Col className="text-center" xs={12} md={5}>
        <Card id="card1" className="p-3 mb-3">
          <Card.Body>
            <Card.Title className="fw-bold">
              {name}
            </Card.Title>
            <Card.Subtitle>
              Product Description:
            </Card.Subtitle>
            <Card.Text>
              {description}
            </Card.Text>
            <Card.Subtitle>
              Price:
            </Card.Subtitle>
            <Card.Text>
              {price}
            </Card.Text>
            <Link id="product-button" className="btn btn-primary" to={`/products/${_id}`}>View Details</Link>
         
          </Card.Body>
        </Card>
      </Col>
    </Row>
    
  )
};
