import {useState, useEffect, useContext} from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function ProductView(){
	const {user} = useContext(UserContext);
	const history = useNavigate();
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(0);
	const [totalAmount, setTotalAmount] = useState(0);
	const [products, setProducts] = useState([]);

	const {productId} = useParams();


	const placeOrder = (productId, quantity) => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/createOrder`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				price: price,
				quantity: quantity,
				totalAmount: totalAmount
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data !== false) {
					setProducts([])
					

				Swal.fire({
					title: 'Successfully placed order!',
					icon: 'success',
					text: 'Thank you for ordering'
				});

				history("/products");
			} else 
			{
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again later'
				});

			}
		})


	};



	useEffect(() => {
		console.log(productId)
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setQuantity(data.quantity);
			setTotalAmount(data.price);
		})
	}, [productId]);

	return (

		<Container className ="mt-5">
			<Row id="product-card" className="mt-3 mb-3">

      
      <Col id="card-text" className="text-center" xs={12} md={5}>
					<Card id="product-view">
						<Card.Body>
							
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>							
							
							

							{ 
							(user.id !== null && user.isAdmin !== true)
							?															
							<Button id="product-button" onClick={() => placeOrder(productId, quantity)}>Checkout</Button>
							:											
							<Button variant="primary" onClick={() => placeOrder(productId, quantity)} hidden>Order</Button>							
							}

							{
							 (user.isAdmin && user.id !== null)
							 ?
							 <>
						
							<Link id="product-button" className="btn btn-primary" to={`/products`}>Back to products</Link>
							 <p id="hyperlink"><Link to="/admin">Admin Dashboard</Link></p>
							</>
							 : 
							 <>

							 <p id="hyperlink"><Link to="/products">Back to Products</Link></p>
		
							<Button variant="danger" as={Link} to={`/admin`} hidden>Admin Dashboard</Button>
							
							</>
							}	

							{
							(user.id === null)
							?
							<Button id="product-button"  as={Link} to={`/login`}>Login</Button>
							:
							<Button id="product-button"  as={Link} to={`/login`} hidden>Login</Button>
						
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>


		)
}



