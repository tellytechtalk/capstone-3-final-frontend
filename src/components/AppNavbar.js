

import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import { Link, NavLink } from 'react-router-dom'
import {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext'
import {Container, NavDropdown, Offcanvas} from 'react-bootstrap';
import {Form, Button, Modal, Row} from 'react-bootstrap';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';




export default function AppNavbar(){

  const {user} = useContext(UserContext)
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);


  return(
    <>
      {[false].map((expand) => (
        <Navbar key={expand} bg="light" expand={expand} className="burgerBtn m-0" sticky="top">
         {/* <Container fluid >*/}

            <OverlayTrigger
                placement="bottom"
                overlay={<Tooltip id="button-tooltip-2">Click to go back to Homepage</Tooltip>}
             >
           <Navbar.Brand className="logo" href="/"><img
              src="/KE_logo6.png"
              width="300"
              height="80"
              className="d-inline-block align-top"
              alt="Kuya Ed's logo"
            /></Navbar.Brand>
          </OverlayTrigger>
    

            <div class="nav navbar-nav d-flex ml-80">

            <Button id = "navbar-btn" className = "orderNow" variant="danger" size= "lg" a href="/products">ORDER NOW</Button>
             </div>
         
             <OverlayTrigger
                placement="bottom"
                overlay={<Tooltip id="button-tooltip-2">See our Services</Tooltip>}
             >

            <Navbar.Toggle id="toggle" aria-controls={`offcanvasNavbar-expand-${expand}`} />

             </OverlayTrigger>


            <Navbar.Offcanvas
               className = "nav-color"
              id={`offcanvasNavbar-expand-${expand}`}
              aria-labelledby={`offcanvasNavbarLabel-expand-${expand}`}
              placement="end"
            >

              <Offcanvas.Header closeButton>
             
                <Offcanvas.Title id={`offcanvasNavbarLabel-expand-${expand}`}>
                  <h3>KUYA ED'S</h3>
                  <p>Roasted 
                  and Fried</p> 
                </Offcanvas.Title>
            
                <Form className="d-flex m-3">
                 <Button id= "off-canvas-button" variant="outline-danger" size= "lg" a href="/products" active>ORDER NOW</Button>
               </Form>
          
              </Offcanvas.Header>


              <Offcanvas.Body>
                <Nav className="justify-content-end flex-grow-1 pe-3">

                   
                  <Nav.Link as={Link} to="/"  className="navbar1  border-bottom">HOME</Nav.Link>
                    {/*<Nav.Link as={Link} to="/"  className="navbar1  border-bottom">ABOUT US</Nav.Link>*/}
                  <Nav.Link as={Link} to="/products"  className="navbar1  border-bottom">MENU</Nav.Link>  


                  { (user.isAdmin) ?
                  <Nav.Link as={NavLink} to="/adminDashboard" className="navbar1 border-bottom">ADMIN DASHBOARD</Nav.Link>
                  :
                  <>
                  </>
                   }

                  { (user.id) ?
                  <>
                  <Nav.Link as={Link} to="/profile" className="navbar1 border-bottom"> MY PROFILE</Nav.Link>
                {/*  <Nav.Link as={NavLink} to="/cart" className="navbar1 border-bottom" disabled>MY CART</Nav.Link>*/}
                  <Nav.Link as={NavLink} to="/logout" className="navbar1 border-bottom ">LOGOUT</Nav.Link>
                  </>
                  :
                  <>
                {/*  <Nav.Link as={NavLink} to="/cart" onClick={handleShow} className="navbar1 border-bottom">MY CART</Nav.Link>*/}

                  {/*<Modal show={show} onHide={handleClose}>
                          <Modal.Body className="text-center">Your cart is currently empty.</Modal.Body>
                          <Modal.Footer>
                            <Button className="btnWhite" a href="/products" variant="dark" onClick={handleClose}>
                              CONTINUE SHOPPING
                            </Button>
                          </Modal.Footer>
                    </Modal>*/}
                      <Nav.Link as={NavLink} to="/login" className="navbar1  border-bottom">LOGIN | REGISTER </Nav.Link>
                  {/*  <Nav.Link as={NavLink} to="/register" className="navbar1  border-bottom">REGISTER</Nav.Link>*/}

                  </>
                }
                  <Nav.Link as={Link} to="/contactUs">BE OUR PARTNER</Nav.Link>
               
                </Nav>
               
              </Offcanvas.Body>
            </Navbar.Offcanvas>
          {/*</Container>*/}
        </Navbar>
      ))}
    </>
  );
}


