import { Row, Col, Card, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom';

export default function Highlights(){
	return(
		<Row>
		    <Col xs={12} md={4}>
		        <Card className="cardHighlight p-3" 
		        	bg="dark"
		        	text="white">
		            <Card.Body>
		                <Card.Title id="card-header" className= "mb-5">
		                    <h2 className= "card3 text-center">MISSION and VISION</h2>
		                </Card.Title>
		                <Card.Text>
		                    To be one of the most satisfying restaurant that offers variety of food party and solo food choices in an affordable yet quality based products
		                </Card.Text>
		            </Card.Body>
		        </Card>
		    </Col>
		    <Col xs={12} md={4}>
		        <Card className="cardHighlight p-3"
		       		 bg="dark"
		        	text="white"
		        	>
		            <Card.Body>
		                <Card.Title id= "card-header" className= "mb-5">
		                    <h2 className= "card3 text-center">AWARDS</h2>
		                </Card.Title>
		                <Card.Text>
		                   CEO Edison Tan founder and owner of Kuya Eds Roasted and Fried received an award as the
						“Most Remarkable and Inspiring Business Leader of the Year” given by Asia’s Golden Award 2022 at Okada Manila
		                </Card.Text>
		            </Card.Body>
		        </Card>
		    </Col>
		    <Col xs={12} md={4}>
		        <Card className="cardHighlight p-3"
		        	bg="dark"
		        	text="white">
		            <Card.Body>
		                <Card.Title id="card-header" className= "mb-5">
		                    <h2 className= "card3 text-center">BE OUR PARTNER</h2>
		                </Card.Title>
		                <Card.Text>
		                    Kuya Ed’s offers opportunity for all aspiring entrepreneurs who wants to take their leap of faith and invest in our various packages which comes in different prices in accordance with your goal, needs and wants.
		                </Card.Text>
		            </Card.Body>
		        </Card>
		    </Col>
		</Row>
	)
}
