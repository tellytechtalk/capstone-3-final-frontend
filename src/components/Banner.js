import {Button, Row, Col} from 'react-bootstrap'
import {Link} from 'react-router-dom';
import Carousel from 'react-bootstrap/Carousel';

/*export default function Banner(){
	return(
		<Row>
			<Col className="p-5 text-center">
				<h1 id="shop-name"></h1>
				<p id="shop-tagline"></p>
				<Button id="homepage-button" variant="primary" as={Link} to="/products">Shop now!</Button>
			</Col>
		</Row>
	)
}
*/

export default function Banner() {
  return (

   
<div id="carouselBanner" class="carousel slide mb-5 min-vh-100" data-ride="carousel">
    <Carousel>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="../banner3.gif"
          alt="First slide"
        />
        <Carousel.Caption>
          <h3></h3>
          <p></p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="../banner4.gif"
          alt="Second slide"
        />
        <Carousel.Caption>
          <h3></h3>
          <p></p>
        </Carousel.Caption>
      </Carousel.Item>

       <Carousel.Item>
        <img
          className="d-block w-100"
          src="../banner.gif"
          alt="Second slide"
        />
        <Carousel.Caption>
          <h3></h3>
          <p></p>
        </Carousel.Caption>
      </Carousel.Item>

    </Carousel>
   </div>
 
  );
}

