import './App.css';
import {useState, useEffect} from 'react'
import { Container } from 'react-bootstrap'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'

import AppNavbar from './components/AppNavbar'
import ProductView from './components/ProductView'
import Footer from './components/Footer'

import ErrorPage from './pages/ErrorPage'
import Home from './pages/Home'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Order from './pages/Order'
import Products from './pages/Products'
import Register from './pages/Register'
import Profile from './pages/Profile'
import AdminDashboard from './pages/AdminDashboard'
import UpdateProduct from './pages/UpdateProduct'
import AddProduct from './pages/AddProduct'

import {UserProvider} from './UserContext'



function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }


  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        });
      };
    });
  
  }, []);

  return (
    <>
      {/*Provides the user context throughout any component inside of it.*/}
      <UserProvider value={{user, setUser, unsetUser}}>
        {/*Initializes that dynamic routing will be involved*/}
        <Router>
          <AppNavbar/>
          {/*<Container>*/}
            <Routes>
              <Route path="/" element={<Home/>}/>
              <Route path="/products" element={<Products/>}/>>
              <Route path ="/products/:productId" element={<ProductView/>}/>
              <Route path="/login" element={<Login/>}/>
              <Route path="/register" element={<Register/>}/>
              <Route path="/profile" element={<Profile/>}/>
              <Route path="/logout" element={<Logout/>} />
              <Route exact path="/adminDashboard" element={<AdminDashboard/>}/>
              <Route exact path ="/:productId/update" element={<UpdateProduct/>}/> 
              <Route exact path ="/create" element={<AddProduct/>}/>
              <Route exact path ="/order" element={<Order/>}/>
              <Route path="*" element={<ErrorPage/>}/>
            </Routes>
         {/* </Container>*/}
        </Router>
      </UserProvider>
      <Footer/>
    </>
    
  );
}

export default App;

