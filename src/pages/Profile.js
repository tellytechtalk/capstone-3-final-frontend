import {useState, useEffect} from 'react'
import {Container, Table} from 'react-bootstrap'


export default function Profile() {


	const [user, setUser] = useState({
		firstName: "",
		lastName: "",
		email: "",
		mobileNo: "",
		isAdmin:""

	})

	useEffect(()=>{
    fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(response => response.json())
    .then(result=> {
      console.log(result);
      setUser({
      	firstName: result.firstName,
      	lastName: result.lastName,
      	email: result.email,
      	mobileNo: result.mobileNo,
      	isAdmin: result.isAdmin
      })
  	})
	},[])

	

	return(
		<div>
		<Container id="profile-table" className="md-6 col-lg-6 mx-auto text-center">
			<Table id ="table-profile" striped ="columns">
		      <thead>
		        <tr>
		          <th id="profile-title" colSpan={4} className="text-center">ACCOUNT DETAILS</th>
		          
		        </tr>
		      </thead>
		      <tbody id="formbox">
		        <tr id="profile-details">
		          <td>First Name</td>
		          <td>{user.firstName}</td>
		          
		        </tr>
		        <tr id="profile-details">
		          <td>Last Name</td>
		          <td>{user.lastName}</td>
		        </tr>
		        
		        <tr id="profile-details">
		          <td>Email</td>
		          <td>{user.email}</td>
		        </tr>
		        
		        <tr id="profile-details">
		          <td>Mobile No.</td>
		          <td>{user.mobileNo}</td>
		        </tr>
		        
		      </tbody>
		    </Table>
		 </Container>
		
		</div>
	)
}
