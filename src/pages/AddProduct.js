import {useState, useEffect, useContext} from "react";
import UserContext from "../UserContext";
import {Navigate, useNavigate, Link} from "react-router-dom";

import Swal from "sweetalert2";
import {Button, Form} from "react-bootstrap";



export default function AddProduct(){


	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	//State hooks to store the values of the input fields
	const [name, setProductName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	

	//Check if the values are successfully binded/passed.
	console.log(name);
	console.log(description);
	console.log(price);
	


	
	function addProduct(e){
		//prevents the page redirection via form submit
		e.preventDefault();

				fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
					method: "POST",
					headers:{
						"Content-Type": "application/json",
						Authorization: `Bearer ${localStorage.getItem("token")}`
					},
					body: JSON.stringify({
						name: name,
						description: description,
						price: price,
						
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if(data){
						//Clear input fields
						setProductName("");
						setDescription("");
						setPrice("");
						

						Swal.fire({
							title: "Completed",
							icon: "success",
							text: "Product successfully added!"
						})

						
						navigate("/admin");
					}
					else{
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again."
						})
					}
				})
			}
		

	//State to determine whether submit button is enabled or not.
	const [isActive, setIsActive] = useState(false);

	// To enable the submit button:

	useEffect(()=>{
		if((name !== "" && description !== "" && price !== "")){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	},[name, description, price])



	return(
		(user.isAdmin)
		?
		<>
		<div id="dashboard-title" className="mt-5 mb-3 text-center">
		<h1>ADD PRODUCT</h1>
		</div>
		
			<Form id="update-product-form" className="md-4 col-lg-4 mx-auto"  onSubmit = {(e) => addProduct(e)}>

				<Form.Group controlId="name">
				  <Form.Label>Product Name</Form.Label>
				  <Form.Control type="text" placeholder="Product Name" value={name} onChange={e => setProductName(e.target.value)}/>
				</Form.Group>

				<Form.Group  controlId="description">
				  <Form.Label>Product description</Form.Label>
				  <Form.Control type="text" placeholder="description" value={description} onChange={e => setDescription(e.target.value)}/>
				</Form.Group>

				<Form.Group  controlId="price">
				  <Form.Label>Price</Form.Label>
				  <Form.Control type="number" placeholder="price" value={price} onChange={e => setPrice(e.target.value)}/>
				</Form.Group>

		      
		      {
		      	isActive
		      	?
		      		<div class="col-md-12 text-center">
		      		<Button variant="primary" type="submit" id="submitBtn">
		      		  Submit
		      		</Button>
		      		</div>
		      	:
		      		<div class="col-md-12 text-center">
		      		<Button variant="primary" type="submit" id="submitBtn" disabled>
		      		  Submit
		      		</Button>
		      		</div>


		      }

		      <p className="text-center"><Link to="/adminDashboard">Back to Admin Dashboard</Link></p>
		    </Form>
    </>

		:
		<Navigate to="/products" />
	)
}
