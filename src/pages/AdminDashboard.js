import { useContext, useState, useEffect } from "react";
import {Container,Table, Button} from "react-bootstrap";
import {useParams, Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";
import {  faGaugeHigh,faUserEdit, faTag, faReceipt, faHandHoldingDollar, faPenToSquare, faSignOut } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";



import Swal from "sweetalert2";

export default function AdminPage(){

	const {productId} = useParams();
	const {user} = useContext(UserContext);
	const [allProducts, setAllProducts] = useState([]);



	const getData = () =>{
		
		fetch(`${process.env.REACT_APP_API_URL}/products/`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllProducts(data.map(product => {
				return(
					<tr id="admin-table" key={product._id}>
						<td>{product._id}</td>
						<td>{product.name}</td>
						<td>{product.description}</td>
						<td>{product.price}</td>
						{/*<td>{product.quantity}</td>*/}
						<td>{product.isActive ? "Active" : "Inactive"}</td>
						<td>
							{
								(product.isActive)
								?	
								 	
								<Button id="dashboard-button-archive" variant="danger" onClick ={() => archive(product._id, product.name)}>Deactivate</Button>
								:
								<>	
								<Button id="dashboard-button-update" variant="success" onClick ={() => activate(product._id, product.name)}>Reactivate</Button>
								
								</>

							}
							<Button id="dashboard-editbutton" as={Link} to={`/${product._id}/update`} >Edit</Button>
						</td>
					</tr>
				)
			}))

		})
	}
	

	//archiving product
	const archive = (productId, productName) =>{
		console.log(productId);
		console.log(productName);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`,{
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Archive Succesful!",
					icon: "success",
					text: `${productName} is now inactive.`
				})
				getData();
			}
			else{
				Swal.fire({
					title: "Archive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	//Making the product active
	const activate = (productId, productName) =>{
		console.log(productId);
		console.log(productName);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/activate`,{
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Activate Succesful!",
					icon: "success",
					text: `${productName} is now active.`
				})
				getData();
			}
			else{
				Swal.fire({
					title: "Activate Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	useEffect(()=>{
		
		getData();
	}, [])

	return(
		(user.isAdmin)
        ?
        <>
            <div className="mt-5 mb-3 text-center">
                <h1 id="dashboard-title">ADMIN DASHBOARD</h1>

                <Button id="dashboard-btn" as={Link} to="/create" variant="success"><FontAwesomeIcon icon={faPenToSquare}></FontAwesomeIcon> ADD PRODUCT</Button>
                <Button id="dashboard-btn" as={Link} to="/products" variant="success"><FontAwesomeIcon icon={faTag}></FontAwesomeIcon> PRODUCTS</Button>
                <Button id="dashboard-btn" as={Link} to="/order" variant="success"><FontAwesomeIcon icon={faReceipt}></FontAwesomeIcon> VIEW ORDERS</Button>
                <Button id="dashboard-btn" as={Link} to="/logout" variant="success"><FontAwesomeIcon icon={faSignOut}></FontAwesomeIcon> LOGOUT</Button>

            </div>

            
            <Table striped bordered hover variant="light" id="admin-table">
           {/* <Container>*/}
             <thead id="admin-table-head">
               <tr >
                 <th>PRODUCT ID</th>
                 <th>PRODUCT NAME</th>
                 <th>DESCRIPTION</th>
                 <th>PRICE</th>
                 <th>STATUS</th>
                 <th>ACTION</th>
               </tr>
             </thead>
             <tbody >
               { allProducts }
             </tbody>
             {/*</Container>*/}
           </Table>
          
        </>
        :
        <Navigate to="/adminDashboard" />
    )
}



