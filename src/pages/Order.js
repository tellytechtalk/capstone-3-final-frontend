import { useContext, useState, useEffect } from "react";
import {Table, Button} from "react-bootstrap";
import {Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";



export default function Orders(){

	
	const {user} = useContext(UserContext);

	const [allOrders, setAllOrders] = useState([]);

	const fetchData = () =>{
		
		fetch(`${process.env.REACT_APP_API_URL}/orders/showOrders`,{
			method: "GET",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllOrders(data.map(order => {
				return(
					<tr id="admin-table" key={order._id}>
						<td>{order._id}</td>
						<td>{order.userId}</td>
						<td>{order.totalAmount}</td>
						<td>{order.purchasedOn}</td>
					</tr>
				)
			}))

		})
	}


	
	

	
	useEffect(()=>{
		
		fetchData();
	}, [])

	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">

				<h1 id="dashboard-title">ORDERS</h1>
				<p><Link to="/adminDashboard"> Admin Dashboard</Link></p>
				
			</div>

			<Table striped>
		     <thead id="admin-table-head"> 
		       <tr id="order-table-order">
		         <th>ORDERS ID</th>
		         <th>USERS</th>
		         <th>TOTAL AMOUNT</th>
		         <th>DATE ORDERED</th>
		       </tr>
		     </thead>
		     <tbody>
		       {allOrders}
		     </tbody>
		   </Table>
		</>
		:
		<Navigate to="/products" />
	)
}

