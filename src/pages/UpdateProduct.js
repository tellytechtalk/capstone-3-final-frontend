import { useContext, useEffect, useState } from 'react';
import { useParams, Navigate } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import UserContext from "../UserContext";
import {Link} from 'react-router-dom';

import Swal from "sweetalert2";

export default function UpdateUser() {


    

    const { productId } = useParams();

    const {user} = useContext(UserContext);
    const [name, setName] = useState();
    const [description, setDescription] = useState();
    const [price, setPrice] = useState();

  
    
    useEffect(() => {


        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
        .then((response) => response.json())
        .then(data => {
            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);

            console.log(data);  
        })
    }, [productId]);



    const updateProduct = async () => {


        let result = await fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`,{
            method:'PATCH',
            body:JSON.stringify(
                {name:name,description:description,price:price}
            ),
            headers:{
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }
        });
        result = await result.json();


        
    }



    return (
        

        (user.isAdmin)
        ?
        <>
        <div id="admin-title" className="mt-5 mb-3 text-center">
        <h1>UPDATE PRODUCT DETAILS</h1>
        </div>

        <Form id="update-product-form" className="md-4 col-lg-4 mx-auto" onSubmit={event => updateProduct(event)
        } >



        <Form.Group controlId="name" id="form-group-update-product">
                <Form.Label>Product Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Product name"
                    required 
                    value={name}
                    onChange={e => setName(e.target.value)}
                />
            </Form.Group>
            <Form.Group controlId="description">
                <Form.Label>Description</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Description" 
                    required 
                    value={description}
                    onChange={e => setDescription(e.target.value)}
                />
            </Form.Group>
            <Form.Group controlId="price">
                <Form.Label>Price</Form.Label>
                <Form.Control 
                    type="number" 
                    placeholder="Price" 
                    required 
                    value={price}
                    onChange={e => setPrice(e.target.value)}
                />
            </Form.Group>
            <Button id="update-product-btn" type="submit">Save Changes</Button>
            
            <p><Link to="/admin">Back to Admin Dashboard</Link></p>

        </Form>

            
        </>
        

        :
              
        <Navigate to="/admin" />


    )
    
   
}




     

