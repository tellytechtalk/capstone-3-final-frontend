import {Form, Button, Container} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import {useNavigate, Navigate} from 'react-router-dom'
import {Link} from 'react-router-dom';
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Register() {

	const {user, setUser} = useContext(UserContext)
	const navigate = useNavigate()

	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNumber, setMobileNumber] = useState('')

	const [email, setEmail] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')

	const [isActive, setIsActive] = useState(false)



	function registerUser(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`,{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(response => response.json())
		.then(result => {
			if(result === true){
				Swal.fire({
					title: 'Oops!',
					icon: 'error',
					text: 'Email already exist!'
				})
			} else {
				// Register request
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: 'POST',
					headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					email: email,
					mobileNo: mobileNumber,
					password: password1
				})
			})
			.then(response => response.json())
			.then(result => {
				if (result !== false){
					// Clear the output fields after form submission
					setFirstName('')
					setLastName('')
					setMobileNumber('')
					setEmail('')
					setPassword1('')
					setPassword2('')

					Swal.fire({
						title: 'Sucess!',
						icon: 'success',
						text: 'You have successfully registered'
					})
					navigate('/login')
				} else { Swal.fire ({
					title: 'Oops!',
					icon: 'error',
					text: 'Something went wrong!'
				})

				}
			})
				
			}
		})
	}

	function authenticate(event){
		event.preventDefault(event)

		localStorage.setItem('email', email)

		setUser({
			email: localStorage.getItem('email')
		})

		setEmail('')

		navigate('/')
	}



	useEffect(() => {
		if((firstName !== '' && lastName !== '' && mobileNumber.length === 11 && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName, lastName, mobileNumber, email, password1, password2])

	return (
		(user.id !==null) ?
			<Navigate to="/"/>
		:
		   <div>
		    <h3 className='text-center bold'id='formHeader'>CREATE YOUR ACCOUNT</h3>
		   
		    <div className='border container mt-2 md-4 col-lg-4 mx-auto' id='regformBox'>

			<Form id="form-text" className="mx-auto" onSubmit={event => registerUser(event)}>

				<Form.Group controlId="firstName">
			        <Form.Label>First Name</Form.Label>
				        <Form.Control 
				            type="text" 
				            placeholder="Enter First Name"
				            value={firstName} 
				            onChange={event => setFirstName(event.target.value)}
				            required
				        />
			    </Form.Group>

			    <Form.Group controlId="lastName">
			        <Form.Label>Last Name</Form.Label>
				        <Form.Control 
				            type="text" 
				            placeholder="Enter Last Name"
				            value={lastName} 
				            onChange={event => setLastName(event.target.value)}
				            required
				        />
			    </Form.Group>

			    <Form.Group controlId="mobileNumber">
			        <Form.Label>Mobile Number</Form.Label>
				        <Form.Control 
				            type="text" 
				            placeholder="Enter Mobile Number"
				            value={mobileNumber} 
				            onChange={event => setMobileNumber(event.target.value)}
				            required
				        />
			    </Form.Group>

			   	<Form.Group controlId="userEmail">
			        <Form.Label>Email address</Form.Label>
				        <Form.Control 
				            type="email" 
				            placeholder="Enter email"
				            value={email} 
				            onChange={event => setEmail(event.target.value)}
				            required
				        />
			    </Form.Group>

	            <Form.Group controlId="password1">
	                <Form.Label>Password</Form.Label>
	                <Form.Control 
		                type="password" 
		                placeholder="Password" 
		                value={password1} 
				        onChange={event => setPassword1(event.target.value)}
		                required
	                />
	            </Form.Group>

	            <Form.Group controlId="password2">
	                <Form.Label>Verify Password</Form.Label>
	                <Form.Control 
		                type="password" 
		                placeholder="Verify Password" 
		                value={password2} 
				        onChange={event => setPassword2(event.target.value)}
		                required
	                />
	            </Form.Group>

	            {	isActive ? 
	            	<div class="col-md-12 text-center">
	            		<Button variant="primary" type="submit" id="submitBtn">
			            	Submit
			            </Button>
			        </div>
			            :
			        <div class="col-md-12 text-center">
			            <Button variant="primary" type="submit" id="submitBtn" disabled>
			            	Submit
			            </Button>
			        </div>

	            }
	            <p className='text-center'>Already have an account? <Link to="/login">LOG IN</Link></p>
	          
			</Form>
			</div>
  		</div>
  

	)
}
